(function(){
  function toggleNav() {
    $('.mobile-toggle').toggleClass('mobile-toggle--active');
    $('.general-nav ul').toggleClass('is-active');
    $('body').toggleClass('is-disabled');
  }
  $('.mobile-toggle').click(toggleNav);

  $('.general-nav__link, .section-hero .button').on('click', function(e) {
    if ($('.mobile-toggle').hasClass('mobile-toggle--active')) {
      toggleNav();
    }

		if(this.hash !== '') {
			e.preventDefault();
			var hash = this.hash;

			$('html, body').animate({
				scrollTop: $(hash).offset().top
			}, 500, function() {
			});
		}
	});

  var vids = [];
  var vidsCalls = 0;
  $.getJSON('../json/vids.json', function(data) {
    $.each(data, function(key, val) {
      vids.push(val.url);
    });

    // задаем максимально возможное кол-во вызовов
    vidsCalls = vids.length / 3;
  });

  var imgs = [];
  var imgsCalls = 0;
  $.getJSON('../json/imgs.json', function(data) {
    console.log(data);
    $.each(data, function(key, val) {
      imgs.push(val.url);
    });

    // задаем максимально возможное кол-во вызовов
    imgsCalls = imgs.length / 3;
  });

  var galleryVids = $('.gallery--vids');
  $('.show--vids').click(function() {
    // возвращаем ф-ию, если вызовов больше нет
    if (vidsCalls <= 0) {
      return;
    }

    for (var i = 0; i < 3; i += 1) {
      // если элементов в массиве больше нет, выходим из цикла (на случай, если кол-во элементов не кратно трем)
      if (vids.length === 0) {
        break;
      }

      galleryVids.append(`<div class="gallery__el gallery__el--vid">${vids[0]}</div>`);
      vids.shift();

      if (!vids.length) {
        $(this).remove();
      }
    }
    vidsCalls--;
  });

  var galleryImgs = $('.gallery--imgs');
  $('.show--imgs').click(function() {
    // возвращаем ф-ию, если вызовов больше нет
    if (imgsCalls <= 0) {
      return;
    }

    for (var i = 0; i < 3; i += 1) {
      // если элементов в массиве больше нет, выходим из цикла (на случай, если кол-во элементов не кратно трем)
      if (imgs.length === 0) {
        break;
      }

      galleryImgs.append(`<div class="gallery__el"><img src="${imgs[0]}"></div>`);
      imgs.shift();

      if (!imgs.length) {
        $(this).remove();
      }
    }
    imgsCalls--;
  });

  function sendMessage(e){
    e.preventDefault();
    var error = false;
    if ( $('#user-email').val().length < 1 ){
      $('#user-email').addClass('field--error');
      error = true;
    }
    if ( $('#user-name').val().length < 1 ){
      $('#user-name').addClass('field--error');
      error = true;
    }
    if ( $('#user-msg').val().length < 1 ){
      $('#user-msg').addClass('field--error');
      error = true;
    }

    if ( error == true ){
      return false;
    }

    $.ajax({
      method: "POST",
      url: "/_inbox/ajax.php",
      data: { email: $('#user-email').val(), name: $('#user-name').val(), text: $('#user-msg').val(), test: "hello" }
    }).done(function( msg ) {
      $('#form2').hide();
      $('#form_caption').hide();
      $('#thx').show();
    });
  }

  function onButtonClick() {
    // Add this to a button's onclick handler
    FB.AppEvents.logEvent("sentNewClient");
  }

  $('.send').click(sendMessage);
  $('.send').click(onButtonClick)
}());
