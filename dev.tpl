<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>RoasUp</title>
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <!-- facebook SDK starts -->
  <script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '2023788377861419',
        cookie     : true,
        xfbml      : true,
        version    : 'v2.12'
      });

      FB.AppEvents.logPageView();

    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "https://connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
  </script>
  <!-- facebook SDK ends -->


  <!-- hero section + general header starts -->
  <section class="section section-hero">
    <div class="row section-hero__content">
      <nav class="general-nav">
        <a href="" class="logo"><span class="roas">Roas</span><span class="up">up</span></a>
        <ul>
          <li class="general-nav__el"><a href="#about" class="general-nav__link">Who We Are</a></li>
          <li class="general-nav__el"><a href="#benefits" class="general-nav__link">Why Work With Us</a></li>
          <li class="general-nav__el"><a href="#showcase" class="general-nav__link">We Produce Creatives</a></li>
          <li class="general-nav__el"><a href="#workprocess" class="general-nav__link">How We Work</a></li>
          <li class="general-nav__el"><a href="#partners" class="general-nav__link">Premium Partners</a></li>
          <li class="general-nav__el"><a href="#contact" class="general-nav__link">Contact Us</a></li>
        </ul>
        <button class="mobile-toggle">
          <span></span>
          <span></span>
          <span></span>
        </button>
      </nav>
      <div class="section-hero__content-inner">
        <h1 class="heading heading--l1">Risk Free UA on Facebook</h1>
        <p class="hero-after-heading">with Our Funds and Knowledge</p>
        <a href="#contact" class="button button--main button--shadow">Get started</a>
      </div>
      <p class="section-hero__note text text--mid">Scale audiences of your games with our advanced marketing team on Facebook!</p>
    </div>
  </section>
  <!-- hero section + general header starts -->

  <!-- about section starts -->
  <section class="section section-about" id="about">
    <div class="row">
      <header class="section__header section__header--bigBotMg">
        <h2 class="heading heading--l2">Who We Are</h2>
        <p class="text text--lg">ROASUP is not just a regular marketing agency, but a team of experts from the gaming industry.</p>
      </header>
    </div>
    <div class="row">
      <div class="section-about__el">
        <figure class="section-about__el-icon">
          <img class="icon-first" src="img/whoweare1.png" alt="">
        </figure>
        <h3 class="heading heading--l3">12 years in<br> gaming industry</h3>
        <p class="text">We publish PC, mobile and social games. ROASUP has experience in marketing, analytics and the mobile games industry.</p>
      </div>
      <div class="section-about__el">
        <figure class="section-about__el-icon">
          <img class="icon-second" src="img/whoweare2.png" alt="">
        </figure>
        <h3 class="heading heading--l3">user acquisition<br> experts</h3>
        <p class="text">Our advanced marketing team developed years of UA skills and an in-depth knowledge of Facebook, Google and third-party platforms.</p>
      </div>
      <div class="section-about__el">
        <figure class="section-about__el-icon">
          <img class="icon-third" src="img/whoweare3.png" alt="">
        </figure>
        <h3 class="heading heading--l3">in-house design<br> and development</h3>
        <p class="text">We develop high-quality creatives with professional designers. ROASUP uses and is constantly improving our AI-based innovative ad-optimization technology.</p>
      </div>
    </div>
  </section>
  <!-- about section ends -->

  <!-- section more starts -->
  <section class="section-more">
    <div class="row">
      <img class="alogo" src="img/alogo.png" alt="">
      <div class="section-more__desc">
        <h3 class="section-more__heading">ROASUP is Achievers Hub Marketing Partner</h3>
        <p class="text">Achievers Hub is a platform for indie teams, investors, publishers and industry professionals.</p>
      </div>
      <a href="#" class="section-more__link">Learn more</a>
    </div>
  </section>
  <!-- section more ends -->

  <!-- section benefits starts -->
  <section class="section section-benefits" id="benefits">
    <div class="row">
      <header class="section__header section__header--bigBotMg">
        <h2 class="heading heading--l2">Why Work With Us</h2>
        <p class="text text--lg">We offer a complete and performance-based UA solution for mobile games.</p>
      </header>
    </div>

    <div class="row">
      <div class="section-benefits__half">
        <div class="section-benefits__el">
          <img class="icon-first" src="img/icon1.svg" alt="">
          <p class="text text--mid">We use our own Facebook accounts and<br> invest our own money into<br> ad campaigns for your games.</p>
        </div>
        <div class="section-benefits__el">
          <img class="icon-second" src="img/icon2.svg" alt="">
          <p class="text text--mid">We focus on mobile games only and<br> bring all our experience in the gaming<br> industry to boost your games.</p>
        </div>
      </div>
      <div class="section-benefits__half">
        <div class="section-benefits__el">
          <img class="icon-third" src="img/icon3.svg" alt="">
          <p class="text text--mid">Our design experts develop images and<br> videos for games to engage the most<br> valuable users.</p>
        </div>
        <div class="section-benefits__el">
          <img class="icon-fourth" src="img/icon4.svg" alt="">
          <p class="text text--mid">We use our own AI-powered<br> ad-optimization technology to deliver<br> the highest ROI results.</p>
        </div>
      </div>
    </div>

    <div class="row">
      <p class="section__note text--xl">We are ready to invest up to $1M per month marketing your games!</p>
    </div>
  </section>
  <!-- section benefits starts -->

  <!-- section showcaase starts -->
  <section class="section section-showcase" id="showcase">
    <div class="row">
      <header class="section__header section__header--noBotMg">
        <h2 class="heading heading--l2">We Produce Creatives</h2>
      </header>
    </div>

    <div class="row gallery-outer">
      <p class="text text--mid">Video ads is one of the best performing ads format. We produce high-quality gaming videos<br> with animated characters, actors and visual effects.</p>
      <div class="gallery gallery--vids">
        <div class="gallery__el gallery__el--vid">
          <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Froasup%2Fvideos%2F152480418773103%2F&show_text=0&width=560" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
        </div>
        <div class="gallery__el gallery__el--vid">
          <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Froasup%2Fvideos%2F152480135439798%2F&show_text=0&width=560" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
        </div>
        <div class="gallery__el gallery__el--vid">
          <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Froasup%2Fvideos%2F152479622106516%2F&show_text=0&width=560" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
        </div>
      </div>
      <button class="show show--vids">Show more</button>
      <div class="line"></div>
    </div>

    <div class="row gallery-outer">
      <p class="text text--mid">Image ads is a well-performing ads format for users around the World. We produce image ads<br> with our team of experienced artists and designers.</p>
      <div class="gallery gallery--imgs">
        <div class="gallery__el">
          <img src="img/ads_img_1.png" alt="">
        </div>
        <div class="gallery__el">
          <img src="img/ads_img_2.png" alt="">
        </div>
        <div class="gallery__el">
          <img src="img/ads_img_3.png" alt="">
        </div>
      </div>
      <button class="show show--imgs">Show more</button>
    </div>
  </section>
  <!-- section showcaase ends -->

  <!-- work process section starts -->
  <section class="section section--smBotPad section-workprocess" id="workprocess">
    <div class="row">
      <header class="section__header section__header--smBotMg">
        <h2 class="heading heading--l2">How We Work</h2>
        <p class="text text--xl">We invest our funds into marketing of your games and manage ad campaigns</p>
      </header>
    </div>

    <div class="section-workprocess__outer">
      <div class="row">
        <p class="text text--xl section-workprocess__pre">Avoid marketing investments risks with one of our business models:</p>
      </div>

      <div class="row">
        <div class="section-workprocess__el">
          <h3 class="heading">Revenue Share</h3>
          <img class="icon-first" src="img/2icon_1.svg" alt="">
          <p class="text text--mid">You share a real revenue from the delivered users.</p>
        </div>
        <div class="section-workprocess__el">
          <h3 class="heading">LTV-based</h3>
          <img class="icon-second" src="img/2icon_2.svg" alt="">
          <p class="text text--mid">You calculate LTV of the delivered users and share estimated revenue with us. </p>
        </div>
        <div class="section-workprocess__el">
          <h3 class="heading">CPA-based</h3>
          <img class="icon-third" src="img/2icon_3.svg" alt="">
          <p class="text text--mid">You pay us a fixed CPA price for every player, who makes in-game purchases.</p>
        </div>
      </div>
    </div>
  </section>
  <!-- work process section ends -->

  <!-- partners section starts -->
  <section class="section section-partners" id="partners">
    <div class="row">
      <header class="section__header section__header--midBotMg">
        <h2 class="heading heading--l2">Premium Partners</h2>
        <p class="text text--xl">We are proud of working with the following mobile game developers:</p>
      </header>
    </div>
    <div class="row">
      <div class="section-partners__el">
        <img src="img/part1.png" alt="">
      </div>
      <div class="section-partners__el">
        <img src="img/part2.png" alt="">
      </div>
      <div class="section-partners__el">
        <img src="img/part4.png" alt="">
      </div>
      <div class="section-partners__el">
        <img src="img/part5.png" alt="">
      </div>
    </div>
    <div class="row">
      <p class="section__note text--xl">Join our Premium Partners today and get the most valuable users without marketing risks!</p>
    </div>
  </section>
  <!-- partners section ends -->

  <section class="section section-contact" id="contact">
    <div class="row">
      <header class="section__header section__header--smBotMg">
        <h2 class="heading heading--l2">Contact Us</h2>
        <p class="text text--xl">If you have any questions or you would like to try our service, please fill the form:</p>
      </header>
    </div>

    <div class="row">
      <form action="">
        <input class="field field--normal" id="user-name" type="text" name="name" placeholder="Your Name">
        <input class="field field--normal" id="user-email" type="email" name="email" placeholder="Your Email">
        <textarea class="field field--big" id="user-msg" name="text" placeholder="Comment"></textarea>
        <button class="button button--main send">Submit</button>
      </form>
    </div>
  </section>

  <footer class="general-footer">
    <div class="row">
      <nav class="general-nav">
        <a href="" class="logo"><span class="roas">Roas</span><span class="up">up</span></a>
        <ul>
          <li class="general-nav__el"><a href="#about" class="general-nav__link">Who We Are</a></li>
          <li class="general-nav__el"><a href="#benefits" class="general-nav__link">Why Work With Us</a></li>
          <li class="general-nav__el"><a href="#showcase" class="general-nav__link">We Produce Creatives</a></li>
          <li class="general-nav__el"><a href="#workprocess" class="general-nav__link">How We Work</a></li>
          <li class="general-nav__el"><a href="#partners" class="general-nav__link">Premium Partners</a></li>
          <li class="general-nav__el"><a href="#contact" class="general-nav__link">Contact Us</a></li>
        </ul>
      </nav>
    </div>
    <div class="row">
      <p class="text text--xs copyright">© 2018 MyPlayCity, Inc. All Rights<br> Reserved. <a href="https://www.roasup.com/privacy_policy/">Privacy Policy</a></p>
    </div>
  </footer>

  <script src="js/jquery-2.2.0.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
