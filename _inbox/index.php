<?php

if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header('WWW-Authenticate: Basic realm="Hello world!!1"');
    header('HTTP/1.0 401 Unauthorized');
    echo 'Bye!';
    exit;
}
if(!in_array(
    // array($_SERVER['PHP_AUTH_USER'],md5($_SERVER['PHP_AUTH_PW'])),
    array($_SERVER['PHP_AUTH_USER'],$_SERVER['PHP_AUTH_PW']),
    array(array('inbox_manager','L0ngAnd\/ery$trong!'))
)){
    header('WWW-Authenticate: Basic realm="Wrong user/password"');
    header('HTTP/1.0 401 Unauthorized');
    echo 'Bye!';
    exit;
}

// $admin_email = 'mariya@myplaycity.com';
// $admin_email = 'mpc.webdev@gmail.com';
$admin_email = 'sergey@myplaycity.com';
$alert = "";
$messages = array();
if ( isset( $_GET['phpinfo'] ) ) {
	phpinfo(); exit;
} else {
	require_once('dbconn.php');
	
	// actions
	if ( isset( $_GET['action'] ) && $_GET['action'] == 'delete' && isset( $_GET['id'] ) && intval($_GET['id']) > 0 ) {
		if ( !mysql_query( "delete from messages_roasup where id = '".intval( $_GET['id'] )."';"  ) ) {
			$alert .= mysql_error();
		}
	}
	
	if ( $r = mysql_query( "select * from messages_roasup"  ) ) while ( $a = mysql_fetch_assoc( $r ) ) $messages[$a['id']] = $a;
	
	
	// actions
	if ( isset( $_GET['action'] ) && $_GET['action'] == 'send' && isset( $_GET['id'] ) && intval($_GET['id']) > 0 ) {
		$id = intval($_GET['id']);
		if ( isset( $messages[$id] ) ) {
			$subject = 'getappdemo.com message from '. $messages[$id]['name'];
			$headers = 'From: ' . $messages[$id]['email'] . "\r\n";
			if (!mail($admin_email, $subject, $messages[$id]['text'], $headers) ) {
				$alert .= 'error sending message';
			}
		} else {
			$alert .= 'invalid id for message to send';
		}
	}
	
}
?>
<html>
	<head>
		<title>GetAppDemo LP Inbox</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	</head>
	<body>
<div class="page-header">
	<div class="container">	
		<h1> <i class="glyphicon glyphicon-inbox"></i> GetAppDemo LP Inbox</h1>
	</div>
</div>
<div class="container">	
<?
if ( $alert ) {
?>
	<div class="alert alert-warning" role="alert"><?=$alert?></div>
<?
}

if ( !count($messages) ) {
?>
	<div class="alert alert-info" role="alert">Message list is empty.</div>
<?
} else {
?>
	<table class="table table-bordered table-hover">
		<tr>
<?
	foreach ( $messages[0] as $k => $v ) {
?>
			<th><?=$k?></th>
<?
	}
?>
			<th>Actions</th>
		</tr>
<?
	foreach ( $messages as $message ) {
?>
		<tr>
<?
		foreach ( $message as $k => $v ) {
?>
			<td><?
			if ( $k == 'email' ) {
			?><a href="mailto:<?=$v?>"><?=$v?></a><?
			} else {
			?><?=$v?><?
			}
			?></td>
<?
		}
?>
			<td>
				<a class="glyphicon glyphicon-envelope" href="?id=<?=$message['id']?>&action=send" title="Forward to <?=$admin_email?>"></a>
				&nbsp;
				<a class="glyphicon glyphicon-remove" href="?id=<?=$message['id']?>&action=delete" title="Remove message" onclick="return confirm('Are you sure?');"></a>
			</td>
		</tr>
<?
	}
?>
	</table>
<?
}
?>
</div>

	</body>
</html>